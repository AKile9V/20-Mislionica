import { Injectable } from "@angular/core";
import { IJWTTokenStudentData, IJWTTokenTeacherData } from "./models/jwt.model"

@Injectable ({
  providedIn: "root"
})

export class JwtService {
  private static readonly JWT_TOKEN: string = "JWT_TOKEN";

  constructor() {}

  public getToken(): string | null {
    const token: string | null = localStorage.getItem(JwtService.JWT_TOKEN);
    if (!token) {
      return null;
    }
    return token;
  }

  public getDataFromToken(): IJWTTokenStudentData | IJWTTokenTeacherData | null {
    const token = this.getToken();

    if (token == null || token == undefined) {
      this.removeToken();
      return null;
    }

    const tokenData = token.split(".")[1];
    const userDataJSON = window.atob(tokenData);
    const payload: IJWTTokenStudentData | IJWTTokenTeacherData = JSON.parse(userDataJSON);

    return payload;
  }

  public setToken(jwt: string | null): void {
    if(jwt === null)
    {
      jwt = "";
    }
    localStorage.setItem(JwtService.JWT_TOKEN, jwt);
  }

  public removeToken(): void {
    localStorage.removeItem(JwtService.JWT_TOKEN);
  }
}
