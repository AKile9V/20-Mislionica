import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { MarksModel } from '../../models/marks.model';
import { SubjectModel } from '../../models/subject.model';
import { MarksService } from '../../services/marks.service';
import { SubjectsService } from '../../services/subjects.service';

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.css']
})
export class GradesComponent implements OnInit, OnDestroy {
  private classSub!:Subscription;
  private gradeSub!:Subscription;
  allClasses:SubjectModel[] = [];

  public marks! : Observable<MarksModel[]>;

  public values : Map<string,MarksModel[]> = new Map();
  public keys: string[] = [];

  @Input() username : string = "";
  @Input() grade : number = 0;

  constructor(private markService : MarksService, private subjService: SubjectsService) { }

  ngOnInit(): void 
  {
    this.keys = [];
    this.values.clear();
    this.classSub = this.subjService.getAllSubjectsByGrade(this.grade).subscribe((data:SubjectModel[]) => {
      data.forEach((aClass) => {
        this.allClasses.push(aClass);
        this.values.set(aClass.subjectName, []);
        this.keys.push(aClass.subjectName);
      })
    });

    this.marks = this.markService.getMarksForStudent(this.username);
    this.parseMarks();
  }

  ngOnDestroy(): void
  {
    this.gradeSub ? this.gradeSub.unsubscribe() : null; 
    this.classSub ? this.classSub.unsubscribe() : null; 
  }

  parseMarks() : void
  {
    this.gradeSub = this.marks.subscribe(data=>
      {
        data.forEach(item=>
          {
            if(this.values.has(item.subjectNames.subjectName))
            {
              const tmp = this.values.get(item.subjectNames.subjectName);
              tmp?.push(item);
            }
                        else
            {
              const tmp : MarksModel[] = [item];
              this.values.set(item.subjectNames.subjectName, tmp);
              this.keys.push(item.subjectNames.subjectName);
            }
          }); 
      }
  );
  }

  getMarks(subject: string, semester: string, isFinal:boolean) : number[]
  {
      const marks : number[] = [];
      if(this.values.has(subject))
      {
        const markss = this.values.get(subject);
        markss?.forEach(value => {
          if(value.isFinalMark == isFinal && semester == value.semester)
          {
            marks.push(value.mark);
          }     
        });
      }
      return marks;
  }

}
