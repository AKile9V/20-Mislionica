import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild, Input } from '@angular/core';

export interface ShowClassInfo {
  classNumber: Number;
  classType: string;
}

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {
  @Input() classInfoDataFromParent:Number[] = [];
  @Output() showClassNumber = new EventEmitter<ShowClassInfo>();

  constructor() { }

  ngOnInit(): void {
  }

  sendClassNumberAndOption(classNumber:Number, showClassOrExercise: string) {
    this.showClassNumber.emit({classNumber: classNumber, classType: showClassOrExercise});
  }
}
