import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TestTemplate, TestToken, TestTokenType } from 'src/app/utils/testtoken';
import { TestModel } from '../../models/test.model';
import { AuthenticationService } from '../../services/authentication.service';
import { TestsService } from '../../services/tests.service';

@Component({
  selector: 'app-student-tests',
  templateUrl: './studenttests.component.html',
  styleUrls: ['./studenttests.component.css']
})
export class StudentTestsComponent implements OnInit, OnDestroy {
  private sub!: Subscription;

  @Input() username : string = "";
  public testData! : TestModel;
  public test! : Observable<TestModel[]>;
  public unsubmitedTests : TestModel[]  = [];
  public testTokens : TestToken[] = [];
  public testTemplate : TestTemplate[] = [];
  constructor(private testService : TestsService, private authService : AuthenticationService) 
  { 

  }

  //1.  1  /f   2 : $>$=$<%=;"
  ngOnInit(): void {
    this.test = this.testService.getTest(this.username);
    this.sub = this.test.subscribe(data =>
      {

        this.unsubmitedTests = data;
      });
  }

  showTest(testModel : TestModel)
  {
    this.testTemplate = [];
    this.testData = testModel;
        var tasks = testModel.testHash.split(";");
        tasks.forEach(task =>
          {
            var testtask : TestTemplate = new TestTemplate();
            const parts = task.split(':');
            if(parts.length === 2)
            {
              this.parseQuestion(parts[0].split('.')[1],testtask);
              this.parseAnswers(parts[1], testtask);
              this.testTemplate.push(testtask);
            }
          });
  }

  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }

  parseQuestion(template : string, testtemplate : TestTemplate)
  {
    var startIndex = 0;
    for(var i = 0; i < template.length; ++i)
    {
      if(template[i] === '/')
      {
        testtemplate.tokens.push(new TestToken(template.substr(startIndex, i - startIndex),TestTokenType.Text))
        testtemplate.tokens.push(new TestToken(template.substr(i, 2),TestTokenType.Field));
        startIndex = i+2;
        ++i;
      }
      if( i === template.length-1)
      {
        testtemplate.tokens.push(new TestToken(template.substr(startIndex, i - startIndex +1),TestTokenType.Text));
      }
    }
    
  }

  parseAnswers(template : string, testtemplate : TestTemplate)
  {    
    for(var i = 0; i < template.length; ++i)
    {
      if(template[i] === '$')
      {
        testtemplate.tokens.push(new TestToken(template.substr(i+1, 1),TestTokenType.RadioButton));
        ++i;
      }
    }
  }

  submitTest() : void
  {
    const testHash = this.parseTest();
    if(testHash === undefined)
    {
      return;
    }
    this.testService.submitTest(testHash,this.username,this.testData.subjectID, this.testData._id);
  }


  parseTest() : string | undefined
  {
    var hash = "";
    var isError = false;
    this.testTemplate.forEach((task,index)=>{
      hash += index+1; 
      hash += ".";
      task.tokens.forEach(token =>{
        if(token.type !== TestTokenType.RadioButton)
        {
          hash+=token.value;
        }
      });
      hash+=":";
      task.tokens.forEach(token =>{
        if(token.type === TestTokenType.RadioButton)
        {
          hash+="$" + token.value;
        }
      });

      if(task.answer === undefined)
      {
        window.alert("Није дат одговор на сваки задатак из теста");
        isError = true;
      }
      else
      {
        hash+="%" + task.answer.value;
        hash+=";";
      }
    });

    return isError ? undefined : hash;
  }

  change(token : TestToken, index : number)
  {
    this.testTemplate[index].answer = token;
  }
}

