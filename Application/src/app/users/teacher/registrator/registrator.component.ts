import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { MarksModel } from '../../models/marks.model';
import { StudentModel } from '../../models/student.model';
import { SubjectModel } from '../../models/subject.model';
import { MarksService,marksChangeRequest } from '../../services/marks.service';
import { StudentService } from '../../services/student.service';
import { SubjectsService } from '../../services/subjects.service';

interface IMarksFormValue {
  subjectName: string
  firstSemester: string;
  firstFinal: string;
  secondSemester: string;
  secondFinal: string;
}

@Component({
  selector: 'app-registrator',
  templateUrl: './registrator.component.html',
  styleUrls: ['./registrator.component.css']
})
export class RegistratorComponent implements OnInit, OnDestroy {
  private subs:Subscription[] = [];

  @Input() username : string = "";

  allClasses:SubjectModel[] = [];
  allStudents:StudentModel[] = [];
  cacheOldMarks:boolean = false;

  public marks! : Observable<MarksModel[]>;

  public values : Map<string,MarksModel[]> = new Map();
  public keys: string[] = [];
  activeStudent : string = "";

  loginForm : FormGroup[] = [];

  constructor(private studentService: StudentService, private markService:MarksService, private subjService: SubjectsService) {
    
   }

  ngOnInit(): void {
    this.subs.push(this.studentService.getAllStudentsFromTeahcer(this.username).subscribe((data)=>{
      data.forEach((aStudent) => {
        this.allStudents.push(aStudent);
      })
    }));
    this.subs.push(this.subjService.getAllSubjectsByGrade(3).subscribe((data:SubjectModel[]) => {
      
      data.forEach((aSubjs) => {
        this.allClasses.push(aSubjs);
        this.values.set(aSubjs.subjectName, []);
        this.keys.push(aSubjs.subjectName);
        this.loginForm.push(new FormGroup({
          subjectName: new FormControl(aSubjs.subjectName),
          firstSemester: new FormControl([]),
          firstFinal: new FormControl([]),
          secondSemester: new FormControl([]),
          secondFinal: new FormControl([])
        }));
      })
    }));
  }

  ngOnDestroy(): void
  {
    this.subs.forEach((val, index) => this.subs[index] ? this.subs[index].unsubscribe() : null);
  }

  ShowMarksForStudent(username: string) : void
  {
    this.keys = [];
    this.values.clear();
    this.allClasses.forEach((aSubj) =>
    {
      this.values.set(aSubj.subjectName, []);
      this.keys.push(aSubj.subjectName);
    })

    this.activeStudent = username;
    this.marks = this.markService.getMarksForStudent(username);
    this.parseMarks();
  }

  parseMarks() : void
  {
    // this.loginForm.forEach((login, index) => {
    //   this.loginForm[index].reset();
    // })

    this.subs.push(this.marks.subscribe(data=>
      {
        data.forEach(item=>
          {
            const isNew : boolean = false;
            if(this.values.has(item.subjectNames.subjectName))
            {
              const tmp = this.values.get(item.subjectNames.subjectName);
              tmp?.push(item);
            }
            // else
            // {
            //   const tmp : MarksModel[] = [item];
            //   this.values.set(item.subjectNames.subjectName, tmp);
            //   this.keys.push(item.subjectNames.subjectName);
            // }
          });
          //console.log(this.getMarks("Математика", "first",false));
          let i = 0;
          this.values.forEach((key,item) =>{
            this.loginForm[i].get("firstSemester")!.patchValue(this.getMarks(item, "first",false));
            this.loginForm[i].get("firstFinal")!.patchValue(this.getMarks(item, "first",true));
            this.loginForm[i].get("secondSemester")!.patchValue(this.getMarks(item, "second",false));
            this.loginForm[i].get("secondFinal")!.patchValue(this.getMarks(item, "second",true));

            i += 1;
          }
          );
      }
  ));
  this.cacheOldMarks = true;
  }

  getMarks(subject: string, semester: string, isFinal:boolean) : number[]
  {
      const marks : number[] = [];
      if(this.values.has(subject))
      {
        const markss = this.values.get(subject);
        // console.log(markss);
        markss?.forEach(value => {
          if(value.isFinalMark === isFinal && semester === value.semester)
          {
            marks.push(value.mark);
          }
          
        });
      }
      return marks;
  }

  submit(index:number) : void
  {
    const data = this.loginForm[index].value as IMarksFormValue;
    
    let changeRequest = null;

    if(data.firstFinal.length > 1)
    {
      window.alert("Унели сте више од једне закључне за прво полугодиште оцене!");
      return;
    }
    else
    {
      const firstfinalDiff = this.getMarksDifference(data.subjectName,data.firstFinal,this.getMarks(data.subjectName, "first",true),  "first", true);
      changeRequest = this.markService.updateMarksForStudent(this.activeStudent,firstfinalDiff);
      if(changeRequest)
      {
        this.subs.push(changeRequest!.subscribe(()=>{
          //this.ShowMarksForStudent(this.activeStudent);
          // this.loginForm[index].get("firstFinal")?.patchValue(data.firstFinal);
        }));
      }
    }

    if(data.secondFinal.length > 1)
    {
      window.alert("Унели сте више од једне закључне за друго полугодиште оцене!");
      return;
    }
    else
    {
      const secondfinalDiff = this.getMarksDifference(data.subjectName,data.secondFinal,this.getMarks(data.subjectName, "second",true), "second", true);
      changeRequest = this.markService.updateMarksForStudent(this.activeStudent,secondfinalDiff);
      if(changeRequest)
      {
        this.subs.push(changeRequest!.subscribe(()=>{
          //this.ShowMarksForStudent(this.activeStudent);
          //this.loginForm[index].get("secondFinal")?.patchValue(data.secondFinal);
        }));
      }
      
    }

    const firstSemesterDiff = this.getMarksDifference(data.subjectName,data.firstSemester,this.getMarks(data.subjectName, "first",false), "first", false);
    const secondSemesterDiff = this.getMarksDifference(data.subjectName, data.secondSemester,this.getMarks(data.subjectName, "second",false), "second", false);
    
    changeRequest = this.markService.updateMarksForStudent(this.activeStudent,firstSemesterDiff);
      if(changeRequest)
      {
        this.subs.push(changeRequest!.subscribe(()=>{
          //this.ShowMarksForStudent(this.activeStudent);
          // this.loginForm[index].get("firstSemester")?.patchValue(data.firstSemester.trim().split(',')
          // .filter((value)=>{return value.length != 0})
          // .map((value)=> value.trim())
          // .sort()
          // .toString())
        }));
      }

      changeRequest = this.markService.updateMarksForStudent(this.activeStudent,secondSemesterDiff);
      if(changeRequest)
      {
        this.subs.push(changeRequest!.subscribe(()=>{
          //this.ShowMarksForStudent(this.activeStudent);
          // this.loginForm[index].get("secondSemester")?.patchValue(data.secondSemester.trim().split(',')
          // .filter((value)=>{return value.length != 0})
          // .map((value)=> value.trim())
          // .sort()
          // .toString())
        }));
      }
  }

  getMarksDifference(subjectName: string, marks: string, oldMarks: number[], semester:string, isFinal:boolean) : marksChangeRequest[]
  {
    let markDiff : marksChangeRequest[] = []
    const diff : number[] = [];
    const newMarks : number[] = [];
    const x = marks.toString();
    const y =  x.split(",");
    y.forEach(mark=>
      {
        let tmp = mark.trim();
        if(!isNaN(parseInt(tmp)))
        {
          newMarks.push(parseInt(tmp));
        }
      });
    newMarks.sort();
    oldMarks.sort();
    for (let i : number  = 0; i < newMarks.length; i++) 
    {
      for(let j : number = 0; j < oldMarks.length; j++)
      {
          if(newMarks[i] === oldMarks[j])
          {
            newMarks.splice(i,1);
            oldMarks.splice(j,1);
            j--;
            i--;
          }
      }
    }
    newMarks.forEach(mark =>
      {
          markDiff.push(new marksChangeRequest(subjectName,mark,semester,isFinal,true))
      });

    oldMarks.forEach(mark =>
      {
          markDiff.push(new marksChangeRequest(subjectName,mark,semester,isFinal,false))
      });
      if(isFinal == true && semester == "second")
      console.log(markDiff);
    
    return markDiff;
  }

}