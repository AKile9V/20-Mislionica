import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { User } from '../../models/user.model';
import { TeacherService } from "../../services/teacher.service"
import { FullNameValidator, UserNameValidator, EmailValidator, PasswordValidator } from "../../../../app/login-register/validators/login-register.validator";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-teacher-profile',
  templateUrl: './teacherprofile.component.html',
  styleUrls: ['./teacherprofile.component.css']
})
export class TeacherProfileComponent implements OnInit, OnDestroy {
  private subs:Subscription[] = [];

  teacherForm !: FormGroup;
  @Input() teacherInfo !: User | null;

  constructor(private teacherService : TeacherService) { }

  ngOnInit(): void {
    this.teacherForm = new FormGroup({
      fullname: new FormControl({ value: this.teacherInfo?.fullname, disabled: true }, [Validators.required, FullNameValidator]),
      username: new FormControl({ value: this.teacherInfo?.username, disabled: true }, [Validators.required, UserNameValidator]),
      email: new FormControl({ value: this.teacherInfo?.email, disabled: true }, [Validators.required, EmailValidator]),
      password: new FormControl({value: "", disabled: true}, [Validators.required]),
      newPassword: new FormControl({value: "", disabled: true}, [Validators.required, PasswordValidator])
    });
  }
  
  ngOnDestroy(): void
  {
    this.subs.forEach((val, index) => this.subs[index] ? this.subs[index].unsubscribe() : null);
  }

  disablePass : boolean = true;
  private imageToUpload : File | null = null;

  private checkValidation() : boolean {
    let hasError: boolean = false;

    const errorFullname : ValidationErrors | null = this.teacherForm.get("fullname")!.errors;
    const errorUsername : ValidationErrors | null = this.teacherForm.get("username")!.errors;
    const errorEmail : ValidationErrors | null = this.teacherForm.get("email")!.errors;
    const errorOldPassword : ValidationErrors | null = this.teacherForm.get("password")!.errors;
    const errorNewPassword : ValidationErrors | null = this.teacherForm.get("newPassword")!.errors;

    if (errorFullname != null) {
      if (errorFullname?.fullNameValidator){
        window.alert(errorFullname.fullNameValidator.message);
      }
      hasError = true;
    }

    if (errorUsername != null) {
      if (errorUsername?.usernamePartsValidator){
        window.alert(errorUsername.usernamePartsValidator.message);
      } 

      if (errorUsername?.usernameValidator){
        window.alert(errorUsername.usernameValidator.message);
      }
    hasError = true;
   }
    
   if (errorEmail != null) {
    if (errorEmail.emailValidator) {
      window.alert(errorEmail.emailValidator.message);
    }
    hasError = true;
  } 

  if (errorOldPassword != null) {

    if(errorOldPassword.required) {
      window.alert("Поље за унос старе шифре није попуњено.");
    }

    hasError = true;
  }

  if (errorNewPassword != null) {

    if(errorNewPassword.required) {
      window.alert("Поље за унос нове шифре није попуњено.");
    }

    if (errorNewPassword.passwordBlankoValidator){
      window.alert(errorNewPassword.passwordBlankoValidator.message);
    }

    if (errorNewPassword.passwordValidator){
      window.alert(errorNewPassword.passwordValidator.message);
    }

    hasError = true;
  } 

    return hasError;
  }

  enableChangeFields() : void {
    this.teacherForm.get("username")?.enable();
    this.teacherForm.get("fullname")?.enable();
    this.teacherForm.get("email")?.enable();
  }

  disableChangeFields() : void{
    this.teacherForm.get("username")?.disable();
    this.teacherForm.get("fullname")?.disable();
    this.teacherForm.get("email")?.disable();
  }

  enableChangePassword() : void {
    this.teacherForm.get("password")?.enable();
    this.teacherForm.get("newPassword")?.enable();
  }

  disableChangePassword() : void {
    this.teacherForm.get("password")?.disable();
    this.teacherForm.get("newPassword")?.disable();
  }
 
  getProfileImage() : string | undefined {
    return this.teacherInfo?.imgUrl;
  }

  public handleFileInput(event: Event): void {
    const files : FileList | null = (event.target as HTMLInputElement).files;
    if (!files!.length) {
      return;
    }
    this.imageToUpload = files!.item(0);

    this.subs.push(this.teacherService.postTeacherProfileImage(this.imageToUpload, this.teacherInfo?.username)!.subscribe());
  }

  public onSubmitUserForm() : void {
    if (this.checkValidation() !== false) { return; }

    this.subs.push(this.teacherService.postUserData(this.teacherForm.get("fullname")?.value, 
                                                    this.teacherInfo!.username, 
                                                    this.teacherForm.get("email")?.value, 
                                                    this.teacherForm.get("username")?.value)!.
                                                    subscribe((user : User) => {
                                                      this.teacherForm.get("fullname")?.patchValue(user.fullname);
                                                      this.teacherForm.get("username")?.patchValue(user.username);
                                                      this.teacherForm.get("email")?.patchValue(user.email);
    }));
    this.disableChangeFields();
  }

  public onSubmitUserFormPassword() : void {
    if (this.checkValidation() !== false) { return; }

    this.subs.push(this.teacherService.postUserPassword(this.teacherForm.get("username")?.value, 
                                                        this.teacherForm.get("password")?.value, 
                                                        this.teacherForm.get("newPassword")?.value)!.
                                                        subscribe(() => {
                                                          this.teacherForm.get("newPassword")?.patchValue("");
                                                          this.teacherForm.get("password")?.patchValue("");
    }));
    this.disableChangePassword();
  }
}
