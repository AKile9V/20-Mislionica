import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ClassesModel } from '../../models/classes.model';
import { SubjectModel } from '../../models/subject.model';
import { ClassesService } from '../../services/classes.service';
import { SubjectsService } from '../../services/subjects.service';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerComponent implements OnInit, OnDestroy {
  @ViewChild('pictureDiv', { static: true, read: ElementRef }) pictureDiv?:ElementRef;
  showPicture: boolean = false;
  showPdf:boolean = false;
  srcImgSummary:string = "";
  private subClass!:Subscription;
  private subSubj!:Subscription;
  allClasses:ClassesModel[] = [];
  allSubjects:SubjectModel[] = [];
  onlySubjectClasses:ClassesModel[] = [];
  isActive:boolean[] = [];
  summaryActive:boolean = false;
  summaryPdf:string = "";
  // Mocked
  activeSubject:string = "OS301";

  constructor(private classService: ClassesService, private subjectService: SubjectsService) { }

  ngOnInit(): void {
    this.subClass = this.classService.getAllClassesByGrade(3).subscribe((data:ClassesModel[]) => {
      data.forEach((aClass) => {
        this.allClasses.push(aClass);
        this.isActive.push(false);
      })
      this.allClasses.sort((classer) => parseInt(classer.classNumber.toString())).reverse();
      this.onlySubjectClasses = this.allClasses.filter((aClass) => {return aClass.subjectID == this.activeSubject});
    });
    this.subSubj = this.subjectService.getAllSubjectsByGrade(3).subscribe((data:SubjectModel[]) => {
      data.forEach((aSubj) => {
        this.allSubjects.push(aSubj);
      })
      this.summaryPdf = this.allSubjects.filter((aClass) => {return aClass.subjectID == this.activeSubject})[0].plan;
    })
  }

  ngOnDestroy(): void {
    this.subClass ? this.subClass.unsubscribe() : null;
    this.subSubj ? this.subSubj.unsubscribe() : null;
  }

  showImageInCenter(classNumber: Number, activeIndex: number)
  {
    this.isActive.forEach((val, index) => this.isActive[index] = false);
    this.summaryActive = false;
    this.isActive[activeIndex] = true;
    this.showPdf = false;

    this.srcImgSummary = this.onlySubjectClasses[activeIndex].classSummaryImgUrl;
    this.showPicture = this.srcImgSummary ? true : false;
  }

  showPlan(): void
  {
    this.srcImgSummary = "";
    this.isActive.forEach((val, index) => this.isActive[index] = false);
    this.summaryActive = true;
    const plan:string = this.allSubjects.filter((subj) => {return subj.subjectID == this.activeSubject})[0].plan;
    this.showPicture = true;
    this.summaryPdf = plan ? plan : "";
    this.showPdf = this.summaryPdf ? true : false;
  }

  hidePicture() : void
  {
    this.summaryActive = false;
    this.isActive.forEach((val, index) => this.isActive[index] = false);
    this.showPicture = false;
    this.showPdf = false;
  }

  activateSubject(subject: string): void
  {
    this.activeSubject = subject;
    this.onlySubjectClasses = this.allClasses.filter((aClass) => {return aClass.subjectID == this.activeSubject});
    this.summaryPdf = this.allSubjects.filter((aClass) => {return aClass.subjectID == this.activeSubject})[0].plan;
  }
}
