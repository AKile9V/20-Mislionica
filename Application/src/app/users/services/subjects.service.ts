import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SubjectModel } from '../models/subject.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  private readonly getSubjectsForGrade = "http://localhost:8000/api/subjects/grade/";
  private subjects! : Observable<SubjectModel[]>;


  constructor(private http : HttpClient) { }

  getAllSubjectsByGrade(grade : number) : Observable<SubjectModel[]>
  {
    this.subjects = this.http.get<SubjectModel[]>(this.getSubjectsForGrade+grade);
    return this.subjects;
  }
}
