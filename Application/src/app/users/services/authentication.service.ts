import { Injectable } from "@angular/core";
import { Subject, Observable, of } from "rxjs";
//import { LoginUser } from "../../login-register/login/models/login-user.model";
import { User } from "../../users/models/user.model"
import { JwtService } from "../../common/services/jwt.service"
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { IJWTTokenStudentData, IJWTTokenTeacherData } from "../../common/services/models/jwt.model";

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  private readonly userSubject: Subject<User | null> = new Subject<User | null>();

  public readonly loginObsUser: Observable<User | null> = this.userSubject.asObservable();

  private isStudent: boolean = true;

  private readonly urls = 
  {
    loginStudent: "http://localhost:8000/api/students/login/",
    loginTeacher: "http://localhost:8000/api/teachers/login/",
    registerStudent: "http://localhost:8000/api/students/register/",
    registerTeacher: "http://localhost:8000/api/teachers/register/"
  };

  constructor(private http: HttpClient, private jwtService: JwtService) { }

  public loginUser(username: string, password: string, isStudent: boolean): Observable<User | null> 
  {
    this.isStudent = isStudent;
    const url: string = this.isStudent ? this.urls.loginStudent : this.urls.loginTeacher;
    const body = { username, password };
    return this.http.post<{ token: string | null}>(url, body).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: { token: string | null }) => this.mapResponseToUser(response)));
  }

  public logoutUser(): void 
  {
    this.jwtService.removeToken();
    this.userSubject.next(null);
  }

  public registerUser(fullname: string, username: string, teachername: string,
                      email: string, grade: number, password: string, isStudent: boolean ): Observable<User | null> 
  {
    this.isStudent = isStudent;
    const url: string = this.isStudent ? this.urls.registerStudent : this.urls.registerTeacher;
    const body = { fullname, username, teachername, email, grade, password };
    return this.http.post<{ token: string | null }>(url, body).pipe(
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: { token: string | null }) => this.mapResponseToUser(response)));
  }



  public mapResponseToUser(response: { token: string | null }): User | null
  {
    this.jwtService.setToken(response.token);
    return this.sendUserDataIfExists();
  }

  public sendUserDataIfExists(): User | null 
  {
    const payloadData: IJWTTokenStudentData | IJWTTokenTeacherData | null = this.jwtService.getDataFromToken();

    let usertemp: User | null = null;

    if(this.isStudent && payloadData)
    {
      usertemp = new User(payloadData.fullName, payloadData.username, 
        payloadData.teacher, payloadData.email, 
        payloadData.password, payloadData.grade, payloadData.imgUrl);
    }
    else if(!this.isStudent && payloadData)
    {
      usertemp = new User(payloadData.fullName, payloadData.username, null, payloadData.email, payloadData.password, payloadData.grade, payloadData.imgUrl);
    }
    
    const user: User | null = payloadData ? usertemp : null;
    this.userSubject.next(user);
    return user;
  }

  public handleError(error: HttpErrorResponse): Observable<{ token: string | null} > 
  {
    const serverError: { message: string; status: number; stack: string } = error.error.error;
    window.alert(`Дошло је до грешке: ${serverError.message}\nСервер је вратио статусни код: ${serverError.status}`);
    // Error stack
    // console.log(serverError.stack);
    return of({ token: this.jwtService.getToken() });
  }
}
