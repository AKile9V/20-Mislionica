import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../users/models/user.model'
import { AuthenticationService } from '../users/services/authentication.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  private sub: Subscription; 
  public user: User | null = null;
  public isStudent: boolean = true;

  constructor(private auth: AuthenticationService) {
    this.sub = this.auth.loginObsUser.subscribe(
      (user: User | null) => {this.user = user;});

    this.auth.sendUserDataIfExists();
   }

  ngOnInit(): void {
  }

  readValue(isStudent: boolean): void {
    this.isStudent = isStudent;
  }

  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }
}
