export class RegisterAlerts {
    constructor(public fullname_label:string = "Име и презиме", public username_label:string = "Корисничко име", 
                public teachername_label:string = "Име и презиме учитеља/учитељице", 
                public email_label:string = "Имејл адреса родитеља", public password_label:string="Шифра", 
                public error_label:string = "", public grade_label:string = "Разред") {}
}