import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css']
})
export class LoginRegisterComponent implements OnInit {
  @ViewChild("studentRadio") studentRadio!: ElementRef;
  @ViewChild("teacherRadio") teacherRadio!: ElementRef;

  @Output() isStudentEvent = new EventEmitter<boolean>();


  
  slideState:string;
  to_register_title: string = "Нови си овде?";
  to_register_body: string = "Драго нам је што желиш да постанеш део Мислионица породице! Региструј се кликом на дугме испод, унеси тражене податке и уплови у свет забавног учења!";
  to_login_title: string = "Већ имаш налог?";
  to_login_body: string = "Па то је одлична вест! Улогуј се кликом на дугме испод, унеси тражене податке и настави тамо где си стао!";
  login_slide_button : string =  "Улогуј се";
  register_slide_button: string = "Региструј се";
  
  constructor() {
    this.slideState = "cont";
   }

  ngOnInit(): void {

  }

  toggleSlide(): void {
    this.slideState = this.slideState === "cont" ? "cont s-signup" : "cont";
  }

  refreshAndEmitUserRadioValue(isStudent: boolean): void {
    if(this.studentRadio.nativeElement.checked)
    {
      this.login_slide_button = "Улогуј се";
      this.register_slide_button = "Региструј се";
      this.to_register_title = "Нови си овде?";
      this.to_register_body = "Драго нам је што желиш да постанеш део Мислионица породице! Региструј се и уплови у свет забавног учења!";
      this.to_login_title = "Већ имаш налог?";
      this.to_login_body = "Па то је одлична вест! Улогуј се кликом на дугме испод, унеси тражене податке и настави тамо где си стао!";
    }
    else if(this.teacherRadio.nativeElement.checked)
    {
      this.register_slide_button = "Региструјте се";
      this.login_slide_button = "Улогујте се";
      this.to_register_title = "Нови сте овде?";
      this.to_register_body = "Драго нам је што желите да постанете део Мислионица породице. Региструјете се кликом на дугме испод, унесите тражене податке и постаните део Мислионица породице!";
      this.to_login_title = "Већ имате налог?";
      this.to_login_body = "Пријавите се тако што ћете кликом на дугме испод отићи на страницу за пријављивање и унети тражене податке!";
    }

    this.slideState = "cont";

    this.isStudentEvent.emit(isStudent);
  }
}
