import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { LoginAlerts } from './models/login-alerts.model';
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { RequiredValidator } from "../validators/login-register.validator";
import { AuthenticationService } from 'src/app/users/services/authentication.service';
import { Subscription } from 'rxjs';

interface ILoginFormValue {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnChanges, OnDestroy {
  @Input() isStudent!: boolean;

  loginAlerts: LoginAlerts = new LoginAlerts();
  loginSub: Subscription = new Subscription();

  required_username:boolean = false;
  required_password:boolean = false;
  password_reset:string = "Заборавио си шифру? Немој да бринеш ми смо ту да ти помогнемо!";
  login_main_label : string = "Улогуј се";
  loginForm : FormGroup;

  constructor(private authService: AuthenticationService) {
    this.loginForm = new FormGroup({
      username: new FormControl("", [RequiredValidator]),
      password: new FormControl("", [RequiredValidator])
    });
   }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.loginSub ? this.loginSub.unsubscribe() : null;
  }

  ngOnChanges(): void {
    if(this.isStudent)
    {
      this.resetSetings();
      this.login_main_label = "Улогуј се";
      this.password_reset = "";
    }
    else
    {
      this.resetSetings();
      this.login_main_label = "Улогујте се";
      this.password_reset = "";
    }
  }

  loginSubmit() : void {
    if (this.checkValidation() !== false) { return; }

    const data = this.loginForm.value as ILoginFormValue;
    //quickfix
    this.loginSub = this.authService.loginUser(data.username, data.password, this.isStudent).subscribe(()=>{window.location.reload();});
  }

  private checkValidation() : boolean {
    let hasError: boolean = false;  
    let erUs: string;
    let erPas: string;
    let errs :string[] = [];

    const errorUsername : ValidationErrors | null = this.loginForm.get("username")!.errors;
    const errorPassword : ValidationErrors | null = this.loginForm.get("password")!.errors;

    this.required_username = errorUsername != null;
    this.required_password = errorPassword != null;

    if (errorUsername != null && errorUsername.requiredValidator){
      erUs = errorUsername.requiredValidator.message;
      this.loginAlerts.username_label = "*Корисничко име";
      hasError = true;
    }
     else {
      erUs = "";
      this.loginAlerts.username_label = "Корисничко име";
    }

    if (errorPassword != null && errorPassword.requiredValidator){
      erPas = errorPassword.requiredValidator.message;
      this.loginAlerts.password_label = "*Шифра";
      hasError = true;
    } else {
      erPas = "";
      this.loginAlerts.password_label = "Шифра";
    }

    errs.push(erUs);
    errs.push(erPas);

    let newErrs = errs.filter((error) => error.length > 0);

    if(newErrs.length != 0){
      this.loginAlerts.error_label = newErrs[0];
    } else {
      this.loginAlerts.error_label = "";
    }

    return hasError;
  }

  private resetSetings(): void {
    this.required_password = false;
    this.required_username = false;
    this.loginAlerts.error_label = "";
    this.loginAlerts.password_label = "Шифра";
    this.loginAlerts.username_label = "Корисничко име"
    this.loginForm.get("username")!.setValue("");
    this.loginForm.get("password")!.setValue("");
  }
}