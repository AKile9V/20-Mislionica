import express from "express"
import * as controller from '../../Controllers/marks'

const router = express.Router();

router.get('/', controller.getAllMarks);
router.get('/final', controller.getFinalMarks);
router.get('/:username', controller.getMarksForStudent);
router.post('/update', controller.updateMarks);

export default router;