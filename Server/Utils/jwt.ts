import * as jwt from "jsonwebtoken";
const MY_SECRET = 'SECRET';

const generateJWT = (data) => {
  return jwt.sign(data, MY_SECRET, { expiresIn: '7d' });
};

const verifyJWT = (token) => {
  return jwt.verify(token, MY_SECRET);
};

export {
    generateJWT,
    verifyJWT,
};
