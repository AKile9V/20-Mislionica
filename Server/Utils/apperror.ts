export class AppError extends Error
{
    constructor(public status:number = 500, ...args) 
    {
        super(...args);
        this.status = status;
    }
}