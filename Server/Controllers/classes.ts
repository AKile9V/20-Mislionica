import { AppError } from "../Utils/apperror" 
import * as classService from "../Service/classes";

const getClassImageByClassNumberSubjectIDAndGrade = async (req, res, next) => {
    const subjectID = req.body.subjectID;
    const classNumber = req.body.classNumber;
    const grade = req.body.grade;
  
    try 
    {
      if (!subjectID || !classNumber || !grade)
      {
        const error = new AppError(400, "Нисте унели неопходне податке!");
        throw error;
      }
  
      const foundClass = await classService.Classes.getClassImageByClassNumberSubjectIDAndGrade(subjectID, classNumber, grade);
      if (!foundClass) 
      {
        res.status(404).json();
      } 
      else 
      {
        res.status(200).json(foundClass);
      }
    } 
    catch (error) 
    {
      next(error);
    }
};

const getAllClassesByGrade = async (req, res, next) => {
  const grade = req.params.grade;
  try
  {
    const allClasses = await classService.Classes.getAllClassesByGrade(grade);
    if (!allClasses) 
    {
      res.status(404).json();
    } 
    else 
    {
      res.status(200).json(allClasses);
    }
  } 
  catch (error) 
  {
    next(error);
  }
};

export {
    getClassImageByClassNumberSubjectIDAndGrade,
    getAllClassesByGrade
};