import * as validator from "validator";
import * as studentsService from "../Service/students";
import * as teachersService from "../Service/teachers";
import { upload } from "../Controllers/uploadControllers";
import { AppError } from "../Utils/apperror" 

const loginUser = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  try 
  {
    const verifyUser = await studentsService.Student.isUserPasswordValid(username, password);

    if(verifyUser === false)
    {
      const error = new AppError(401, "Погрешна лозинка. Пробајте поново!");
      throw error;
    }
    else if(!verifyUser)
    {
      const error = new AppError(404,`Корисник са корисничким именом "${username}" не постоји!`);
      throw error;
    }

    const jwt = await studentsService.Student.getUserJWTByUsername(username);
    if (!jwt) 
    {
      const error = new AppError(404,`Корисник са корисничким именом "${username}" не постоји!`);
      throw error;
    }

    return res.status(200).json({ token: jwt, });
  } 
  catch (err) 
  {
    next(err);
  }
};

const getAllUsers = async (req, res, next) => {
  try 
  {
    const allUsers = await studentsService.Student.getAllUsers();
    res.status(200).json(allUsers);
  } 
  catch (error) 
  {
    next(error);
  }
};

const getUserByUsername = async (req, res, next) => {
  const username = req.params.username;

  try 
  {
    if (!username)
    {
      const error = new AppError(400, "Нисте унели корисничко име!");
      throw error;
    }

    const user = await studentsService.Student.getUserByUsername(username);
    if (!user) 
    {
      res.status(404).json();
    } 
    else 
    {
      res.status(200).json(user);
    }
  } 
  catch (error) 
  {
    next(error);
  }
};

const getUsersByStatus = async (req, res, next) => {
  const status = req.params.status;

  try 
  {
    if (!status) 
    {
      const error = new AppError(400, 'Нисте задали тренутни статус корисника!');
      throw error;
    }

    const users = await studentsService.Student.getUsersByStatus(status);
    if (!users) 
    {
      res.status(404).json();
    } 
    else 
    {
      res.status(200).json(users);
    }
  } 
  catch (error) 
  {
    next(error);
  }
};

const getUsersByTeacher = async (req, res, next) => {
  const teacher = req.params.teacher;
  
  try 
  {
    if (!teacher) 
    {
      const error = new AppError(400, 'Нисте задали корисничко име учитеља!');
      throw error;
    }

    const users = await studentsService.Student.getUsersByTeacher(teacher);
    if (!users) 
    {
      res.status(404).json();
    } 
    else 
    {
      res.status(200).json(users);
    }
  } 
  catch (error) 
  {
    next(error);
  }
};

const getUsersByGrade = async (req, res, next) => {
  const grade = req.params.grade;

  try 
  {
    if (!grade) 
    {
      const error = new AppError(400, 'Нисте задали разред!');
      throw error;
    }

    const users = await studentsService.Student.getUsersByGrade(grade);
    if (!users) 
    {
      res.status(404).json();
    } 
    else 
    {
      res.status(200).json(users);
    }
  } 
  catch (error) 
  {
    next(error);
  }
};

const addNewUser = async (req, res, next) => {
  const { fullname, username, teachername, email, grade, password } = req.body;

  try 
  {
    if (!username || !email || !password ||
        !validator.isEmail(email) || !validator.isAlphanumeric(username)) 
    {
      const error = new AppError(400 ,`Подаци нису исправни! Молимо Вас проверите прослеђене податке.`);
      throw error;
    }

    const user = await studentsService.Student.getUserByUsername(username);
    if (user) 
    {
      const error = new AppError(403 ,`Корисничко име "${username}" је заузето! Одаберите неко друго.`);
      throw error;
    }

    const userEmail = await studentsService.Student.getUserByEmail(email);
    if (userEmail) 
    {
      const error = new AppError(403 ,`Емаил адреса "${email}" је заузета! Одаберите неку другу.`);
      throw error;
    }

    const teacher = await teachersService.Teacher.getTeacherByFullname(teachername);
    if(!teacher)
    {
      const error = new AppError(404 ,`Учитељ "${teachername}" не постоји!`);
      throw error;
    }

    const jwt = await studentsService.Student.addNewUser(fullname, username, email, password, teacher._id, grade);
    return res.status(201).json({ token: jwt });
  } 
  catch (error) 
  {
    next(error);
  }
};

const changeUserPassword = async (req, res, next) => {
  const { username, oldPassword, newPassword } = req.body;

  try 
  {
    if (!username || !oldPassword || !newPassword) 
    {
      const error = new AppError(400 ,`Подаци нису исправни! Молимо Вас проверите прослеђене податке.`);
      throw error;
    }

    const user = await studentsService.Student.getUserByUsername(username);

    if (!user) 
    {
      const error = new AppError(404 ,`Кориснк са корисничким именом "${username}" не постоји!`);
      throw error;
    }

    const jwt = await studentsService.Student.changeUserPassword(username, oldPassword, newPassword);

    if (jwt) 
    {
      res.status(200).json({ token: jwt });
    } 
    else 
    {
      const error = new AppError(403 ,`Нисте унели добру лозинку!`);
      throw error;
    }
  } 
  catch (error) 
  {
    next(error);
  }
};

const deleteUser = async (req, res, next) => {
  const username = req.params.username;

  try 
  {
    if (!username) 
    {
      const error = new AppError(400 ,`Подаци нису исправни! Молимо Вас проверите прослеђене податке.`);
      throw error;
    }

    const user = await studentsService.Student.getUserByUsername(username);
    if (!user) 
    {
      const error = new AppError(404 ,`Кориснк са корисничким именом "${username}" не постоји!`);
      throw error;
    }

    await studentsService.Student.deleteUser(username);
    res.status(200).json();
  } 
  catch (error) 
  {
    next(error);
  }
};

const changeProfileImage = async (req, res, next) => {
  const username = req.params.username;
  try 
  {
    await upload(req, res);
    const imgUrl = req.file.filename;
    await studentsService.Student.changeProfileImage(username, "http://localhost:8000/Uploads/"+ imgUrl);
    const jwt = await studentsService.Student.getUserJWTByUsername(username);
    if (!jwt) 
    {
      const error = new AppError(404,`Корисник са корисничким именом "${username}" не постоји!`);
      throw error;
    }
    return res.status(200).json({ token: jwt, });
  } 
  catch (error) 
  {
    next(error);
  }
}; 

const changeUserInfoData = async (req, res, next) => {
  const fullname = req.body.fullname;
  const username = req.params.username;
  const email = req.body.email;
  const newUsername = req.body.newUsername;

  try 
  {
    if (!email || !fullname) 
    {
      const error = new AppError(400 ,`Подаци нису исправни! Молимо Вас проверите прослеђене податке.`);
      throw error;
    }

    await studentsService.Student.updateUserData(fullname, username, email, newUsername);
    const jwt = await studentsService.Student.getUserJWTByUsername(req.body.newUsername);

    return res.status(200).json({ token: jwt, });
  } 
  catch (error) 
  {
    next(error);
  }
};

export {
    loginUser,
    getAllUsers,
    getUserByUsername,
    getUsersByStatus,
    getUsersByTeacher,
    getUsersByGrade,
    changeUserPassword,
    addNewUser,
    deleteUser,
    changeProfileImage,
    changeUserInfoData
};