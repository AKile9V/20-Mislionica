import mongoose, { Number } from 'mongoose';

export interface ITestsDoc extends mongoose.Document
{
    _id: string,
    subjectID: string,
    grade: number,
    testHash : string
}

const testsShema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true
    },
    subjectID: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    grade: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    testHash:{
        type: mongoose.Schema.Types.String,
        required: true
    }
},    {
    versionKey: false
    });

const TestModel = mongoose.model<ITestsDoc>('testsData', testsShema);

export default TestModel;