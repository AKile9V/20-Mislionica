import mongoose, { Number } from 'mongoose';

export interface ISubmitedTestsDoc extends mongoose.Document
{
    _id: string,
    subjectID: string,
    username: string,
    testHash: string,
    testID : string
}

const submitedTestsShema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true
    },
    subjectID: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    username: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    testHash:{
        type: mongoose.Schema.Types.String,
        required: true
    },
    testID:{
        type: mongoose.Schema.Types.String,
        required: true
    }
},    {
    versionKey: false
    });

const SubmitedTestsModel = mongoose.model<ISubmitedTestsDoc>('submitedtests', submitedTestsShema);

export default SubmitedTestsModel;