import mongoose from 'mongoose';

export interface ITestDoc extends mongoose.Document
{
    _id: string,
    text : string,
    firstAnswer : string,
    secondAnswer : string,
    thirdAnswer : string
}

const testsShema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true
    },
    text: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    firstAnswer: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    secondAnswer: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    thirdAnswer: {
        type: mongoose.Schema.Types.String,
        required: true
    }
},    {
    versionKey: false
    });

const TestsModel = mongoose.model<ITestDoc>('tests', testsShema);

export default TestsModel;