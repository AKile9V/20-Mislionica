echo "Adding new model: $1"
if [ $1 != '' ]
then
cd Controllers
touch "$1.ts"
echo "- Model added to Controllers"
cd ../Models
touch "$1.ts"
echo "- Model added to Models"
cd ../Service
touch "$1.ts"
echo "- Model added to Service"
cd ../Routes/Api
touch "$1.ts"
echo "- Model added to Routes/Api"
fi
echo "Model $1 has been added to project"