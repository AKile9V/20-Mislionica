echo "Exporting [mislionica] database"
mongoexport --collection=students --db=mislionica --out=DBs/mislionica/students.json
mongoexport --collection=teachers --db=mislionica --out=DBs/mislionica/teachers.json
mongoexport --collection=subjects --db=mislionica --out=DBs/mislionica/subjects.json
mongoexport --collection=marks --db=mislionica --out=DBs/mislionica/marks.json
mongoexport --collection=tests --db=mislionica --out=DBs/mislionica/tests.json
mongoexport --collection=testsdatas --db=mislionica --out=DBs/mislionica/testsdatas.json
mongoexport --collection=submitedtests --db=mislionica --out=DBs/mislionica/submitedtests.json