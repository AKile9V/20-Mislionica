import * as uuid from "uuid"

import ClassesModel from '../Models/classes'

export class Classes
{
    static async getClassImageByClassNumberSubjectIDAndGrade(subjectID:string, classNumber: number, grade: number)
    {
        const findClass = await ClassesModel.findOne({ subjectID: subjectID, classNumber: classNumber, grade: grade }).exec();
        return findClass;
    };

    static async getAllClassesByGrade(grade: number)
    {
        const allClasses = await ClassesModel.find({grade: grade}).exec();
        return allClasses;
    };
}